import os
from setuptools import setup, find_packages

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='viewer',
    version='0.0.0',
    # namespace_packages=['c_models', 'c_models.django_models'],
    # packages=[p for p in find_packages() if p.partition('.')[0] == 'c_models'],
    # include_package_data=True,
    license='Mitaka',
    install_requires=[
        'django==2.0.6',
        'psycopg2==2.7.4',
        'six==1.11.0',
        'pytz==2018.4',
    ],
    description=('This is a viewer/manager for crawler_bot items'),
    author='Dimitar Varbenov',
    author_email='dimitarvarbenov@gmail.com',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
    ],
    zip_safe=True,
)
