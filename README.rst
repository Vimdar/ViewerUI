ViewerUI
===========================

This simple Django project creates simple deployable cms view manage
items collected from crawling.
For storing it creates and manages psql database with django models.
Roles and authentication/rights are to be added.

Installation
------------

-Clone or fork(depending on your needs), setup psql.
(Could also use pgadmin/pgbouncer for easier control if deployed not on localhost.)
-Create a db and run migrations.
-Create local(untracked) file with prod/dev settings you'll use and sync it with settings.
***WARNING***
*Don't commit password in the project's repository.
Import them in `settings.py` like `from secrets import *`
where secrets.py is an untracked file.*
- Run Django web server and use.

Usage
-----
------


**WARNING: This viewer provides create/update/manage/view and manage a certain database.
You must take care and update models if db changes are forced.
This project uses Django 2.0 but no backwards compatibility is guaranteed.

Tests
~~~~~

No tests so far #TODO

Repository
~~~~~~~~~~
----------
