from .models import BotItem
# from django import forms
from django.forms import ModelForm


class UpdateForm(ModelForm):
    # Left if confirm needed
    # def save(self, commit=True):
    #     instance = super(UpdateForm, self).save(commit=False)
    #     if commit:
    #         instance.save(update_fields=['title', 'body', 'original_date', 'author', 'author_id'])
    #     return instance

    class Meta:
        model = BotItem
        fields = ['title', 'body', 'url', 'original_date', 'author', 'author_id']
