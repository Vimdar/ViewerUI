# from django.urls import path
# from django.conf.urls import url

from . import views
from django.urls import path

app_name = 'checker'
urlpatterns = [
    path(r'', views.IndexAll.as_view(), name='index'),
    path(r'item/<int:item_id>/', views.ArticleView.as_view(), name='detail'),
    path(r'item/<int:pk>/update/', views.ArticleUpdate.as_view(), name='update_article'),
    path(r'item/create/', views.ArticleCreate.as_view(), name='create_article'),
    path(r'item/<int:pk>/delete/', views.ArticleDelete.as_view(), name='delete_article'),
]
