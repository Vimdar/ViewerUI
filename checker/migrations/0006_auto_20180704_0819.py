# Generated by Django 2.0.6 on 2018-07-04 08:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('checker', '0005_botitem_source_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='botitem',
            name='url',
            field=models.CharField(max_length=2000, null=True),
        ),
    ]
