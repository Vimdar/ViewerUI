from django.views import generic
from django.views.generic.edit import UpdateView
from django.views.generic import ListView, DetailView, CreateView, DeleteView
from .models import BotItem
from django.shortcuts import reverse
from .forms import UpdateForm


class ArticleView(DetailView):
    queryset = BotItem.objects.filter()
    model = BotItem
    ordering = ['-original_date']
    template_name = 'detail.html'
    pk_url_kwarg = 'item_id'


class IndexAll(generic.ListView):
    template_name = 'index.html'
    context_object_name = 'latest_item_list'
    model = BotItem
    paginate_by = 10
    queryset = BotItem.objects.order_by('-original_date')


class ArticleUpdate(UpdateView):
    model = BotItem
    template_name = 'update_article.html'
    form_class = UpdateForm
    # pk_url_kwarg = 'item_id'

    def get_context_data(self, **kwargs):
        context = super(UpdateView, self).get_context_data(**kwargs)
        return context

    def get_success_url(self, **kwargs):
        return reverse("checker:detail", kwargs={'item_id': self.object.item_id})


class ArticleCreate(CreateView):

    model = BotItem
    template_name = 'update_article.html'
    form_class = UpdateForm

    def get_success_url(self, **kwargs):
        return reverse("checker:index")


class ArticleDelete(DeleteView):
    model = BotItem
    template_name = 'confirm_delete.html'

    def get_success_url(self, **kwargs):
        return reverse("checker:index")
