from __future__ import unicode_literals

from django.db import models
import django.utils.timezone as tz


class BotItem(models.Model):
    item_id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=255, blank=True, null=True)
    body = models.TextField(blank=True, null=True)
    url = models.CharField(max_length=2000)
    original_date = models.DateTimeField(default=tz.now)
    author = models.CharField(max_length=255, blank=True, null=True)
    source_id = models.IntegerField(blank=True, null=True)
    item_id_in_source = models.CharField(max_length=2000)
    author_id = models.CharField(max_length=255, blank=True, null=True)
    # add created_by field for source table

    def __str__(self):
        return self.title

    class Meta:
        # managed = False
        db_table = 'bot_items\".\"items'
